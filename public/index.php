<?php

require 'vendor/autoload.php';



define('PATH','/git/site.viole/public/');
define('PREFIX', ''); //default
define('CONFIG','configtest.json');
define('PASSWORD', 'viole');

require 'api/json.php';
require 'api/file.php';
require 'api/login.php';

Flight::route('/', function(){
    $en = Flight::request()->query['en'];
    if($en == 1){
      Flight::redirect('home');
    }else{
      Flight::redirect('inicio');
    }

});
Flight::route('/inicio(/@active)', function($active){
  $active = isset($active)?$active:'home';
  Flight::render('home'.PREFIX.'.es', array('active' => $active,'lang'=>'es'));
});

Flight::route('/grupos/@id', function($id){
  $id = isset($id)?$id:-1;
  $active = isset($active)?$active:'grupos';
  Flight::render('grupos'.PREFIX.'.es', array('id' => $id,'active' => $active,'lang'=>'es'));
});


Flight::route('/admin', function(){
  Flight::render('admin'.PREFIX.'.es', array('active'=>'admin','lang'=>'es'));
});
Flight::route('/editor', function(){
  $pass = Flight::request()->query['pass'];
  $pass = base64_decode($pass);
  if($pass != PASSWORD){
    Flight::redirect('/inicio');
  }
  $active = isset($active)?$active:'editor';
  Flight::render('editor'.PREFIX.'.es', array('active'=>$active,'lang'=>'es'));
});


Flight::route('/date', function(){
  date_default_timezone_set('America/Argentina/Buenos_Aires');
  echo date('Y_m_d_H_i_s');
  exit;
});

/*
Flight::map('notFound', function(){
    Flight::redirect('/');
});
*/

/*
Flight::route('/home(/@active)', function($active){
  $active = isset($active)?$active:'home';
  Flight::render('home'.PREFIX.'.en', array('active' => $active,'lang'=>'en'));
});

Flight::route('/aboutus', function(){
    Flight::render('aboutus'.PREFIX.'.en', array('active' => 'aboutus','lang'=>'en'));
});
Flight::route('/sobrenosotros', function(){
    Flight::render('aboutus'.PREFIX.'.es', array('active' => 'aboutus','lang'=>'es'));
});
Flight::route('/quienessomos', function(){
    Flight::render('aboutus'.PREFIX.'.es', array('active' => 'aboutus','lang'=>'es'));
});



Flight::route('/servicios', function(){
    Flight::render('services'.PREFIX.'.es', array('active' => 'services','lang'=>'es'));
});
Flight::route('/services', function(){
    Flight::render('services'.PREFIX.'en', array('active' => 'services','lang'=>'en'));
});


Flight::route('/trabajos', function(){
    Flight::render('projects', array('active' => 'projects','lang'=>'es'));
});
Flight::route('/proyectos', function(){
    Flight::render('projects', array('active' => 'projects','lang'=>'es'));
});
Flight::route('/projects', function(){
    Flight::render('projects'.PREFIX, array('active' => 'projects','lang'=>'en'));
});
Flight::route('/works', function(){
    Flight::render('projects'.PREFIX, array('active' => 'projects','lang'=>'en'));
});



Flight::route('/attorneys', function(){
    Flight::render('home'.PREFIX, array('active' => 'team','lang'=>'en'));
});
Flight::route('/nuestrosabogados', function(){
    Flight::render('home'.PREFIX, array('active' => 'team','lang'=>'es'));
});
Flight::route('/equipo', function(){
    Flight::render('home'.PREFIX, array('active' => 'team','lang'=>'es'));
});
Flight::route('/team', function(){
    Flight::render('home'.PREFIX, array('active' => 'team','lang'=>'es'));
});


Flight::route('/contacto', function(){
    Flight::render('contact'.PREFIX.'.es', array('active' => 'contact','lang'=>'es'));
});
Flight::route('/contact', function(){
    Flight::render('contact'.PREFIX.'.en', array('active' => 'contact','lang'=>'en'));
});
*/


//Config
//Flight::set('flight.views.path', '/path/to/views');

Flight::start();

 ?>
