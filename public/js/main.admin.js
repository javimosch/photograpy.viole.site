console.info('main.admin');
eventable(window);
var save = null,
    login = null;
//LOGIN
if (document.querySelectorAll('.password').length === 1) {
    var input = document.querySelectorAll('.password')[0];
    login = function() {
        var pass = input.value;
        ajax(site.path + 'login', function(r) {
            r = JSON.parse(r);
            console.info(r);
            if (r.ok) {
                Lockr.set('pass', btoa(pass))
                window.location.href = site.path + 'editor?pass=' + btoa(pass);
            } else {
                $('.password').val('').attr('placeholder', 'Incorrecto').focus();
                setTimeout(function() {
                    $('.password').attr('placeholder', 'Password');
                }, 2000);
            }
        }, {
            pass: pass
        });
    }
    snippet_evt_on(input, 'keydown', function(e) {
        if (e.keyCode === 13) {
            login();
        }
    })
    if (Lockr) {
        var pass = (Lockr.get('pass') !== undefined) ? atob(Lockr.get('pass')) : '';
        input.value = pass;
        if (pass !== '') login();
    }
}


window.$on('filedrop.init', function() {
    var options = {
        iframe: {
            url: 'upload.php'
        }
    };
    // Attach FileDrop to an area ('zone' is an ID but you can also give a DOM node):
    var zone = new FileDrop('zone', options);
    // Do something when a user chooses or drops a file:
    zone.event('send', function(files) {
        // Depending on browser support files (FileList) might contain multiple items.
        files.each(function(file) {
            // React on successful AJAX upload:
            file.event('done', function(xhr) {
                // 'this' here points to fd.File instance that has triggered the event.
                alert('Done uploading ' + this.name + ', response:\n\n' + xhr.responseText);
            });
            // Send the file:
            file.sendTo(window.site.path+'file/upload');
        });
    });
    zone.event('iframeDone', function(xhr) {
        alert('Done uploading via <iframe>, response:\n\n' + xhr.responseText);
    });
    fd.addEvent(fd.byID('multiple'), 'change', function(e) {
        zone.multiple(e.currentTarget || e.srcElement.checked);
    });
});






//EDITOR
var container = document.getElementById("jsoneditor");
if (container) {
    var editor = new JSONEditor(container);
    // set json
    var json = {
        "Array": [1, 2, 3],
        "Boolean": true,
        "Null": null,
        "Number": 123,
        "Object": {
            "a": "b",
            "c": "d"
        },
        "String": "Hello World"
    };
    editor.set(site.config);
    // get json
    save = function() {
        var json = editor.get();
        ajax(site.path + 'editor/save', function(r, xhr) {
            r = JSON.parse(r);
            console.info(r);
            if (r.ok) {
                $('.editor label').fadeIn().delay(800).fadeOut();
            }
        }, {
            json: json,
            filename: 'configtest.json'
        });
    };
    window.$emit('filedrop.init');
}
