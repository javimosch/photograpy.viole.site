//$('body').awesomeCursor('chevron-right');
$(function() {
    var self = {
        photoContainer: '.photoContainer',
        photoSlideBoxContainer: 'slide-boxes',
    };
    eventable(self);
    self.$images = $(self.photoContainer + ' img');
    self.$active = $(self.photoContainer + ' img').eq(0);

    function onresizeSlideBoxes() {
        var $cont = $('.' + self.photoSlideBoxContainer),
            $left = null,
            $right = null;
        if ($cont.length == 0) {
            $cont = $("<div class='" + self.photoSlideBoxContainer + "'/>").prependTo($(self.photoContainer));
            var $left = $("<div class='left'/>").appendTo($cont);
            var $right = $("<div class='right'/>").appendTo($cont);
        } else {
            $right = $('.' + self.photoSlideBoxContainer + ' .right');
            $left = $('.' + self.photoSlideBoxContainer + ' .left');
        }
        if (typeof self._slideBoxesBinded === 'undefined' || !self._slideBoxesBinded) {
            snippet_evt_on($left.get(0), 'mouseenter', function() {
                $('body').awesomeCursor('chevron-left');
            });
            snippet_evt_on($left.get(0), 'click', function() {
                self.$emit('slide.left');
            });
            snippet_evt_on($right.get(0), 'mouseenter', function() {
                $('body').awesomeCursor('chevron-right');
            });
            snippet_evt_on($right.get(0), 'click', function() {
                self.$emit('slide.right');
            });
            snippet_evt_on($('body').get(0), 'mouseenter', function() {
                $('body').css('cursor', '');
            });
            self._slideBoxesBinded = true;
        }
    }
    self.$on('init.home', function() {

    });
    self.$on('init.grupos', function() {
        snippet_evt_on(window, 'resize', function() {
            onresize();
        });
        onresize();
        setActive(0);
        window.self = self;
    });
    self.$on('slide.left', function() {
        moveActive(-1);
    });
    self.$on('slide.right', function() {
        moveActive(1);
    });

    function moveActive(direction) {
        self.$images.each(function(i, v) {
            if ($(this).attr('class').indexOf('active') != -1) {
                setActive(i + direction);
                return false;
            }
        })
    }

    function setActive(index) {
        self.$active.removeClass('active');
        if (index < 0) {
            index = self.$images.length - 1;
        } else {
            if (index == self.$images.length) index = 0;
        }
        self.$images.eq(index).addClass('active');
        self.$active = self.$images.eq(index);
        resizeimage(self.$active);
        $('footer p').html(self.$active.attr('alt'));
        $('.info p').html((index + 1).toString() + '/' + self.$images.length);
    }

    function resizeimage($img) {
        var $cont = $img.parent();
        var v = {
            w: $cont.width(),
            h: $cont.height()
        };
        var size = snippet_scale_restrictAuto($img[0].naturalWidth, $img[0].naturalHeight, v.w, v.h);
        $img.css({
            width: size.w,
            height: size.h
        });
    }

    function onresize() {
        var $cont = $(self.photoContainer);
        $cont.css({
            'width': $(window).width() / 1.1,
            'height': $(window).height()
        });
        $(self.photoContainer + ' img').each(function() {
            //resizeimage($(this));
        });
        $cont.css({
            'left': $(window).width() / 2 - $cont.width() / 2
        });
        onresizeSlideBoxes();
    }
    self.$emit('init.'+site.active);
});