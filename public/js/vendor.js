function ajax(url, callback, data, settings) {
    try {
        settings = settings || {};
        settings.type = settings.type || 'application/json;charset=UTF-8;' //'application/x-www-form-urlencoded';
        var x = new(this.XMLHttpRequest || ActiveXObject)('MSXML2.XMLHTTP.3.0');
        x.open(data ? 'POST' : 'GET', url, 1);
        x.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        x.setRequestHeader('Content-type', settings.type);
        x.onreadystatechange = function () {
            x.readyState > 3 && callback && callback(x.responseText, x);
        };
        x.send(JSON.stringify(data));
    } catch (e) {
        window.console && console.log(e);
    }
};

function snippet_scale_restrictW(w, h, r) {
    var rta = {
        w: w,
        h: h
    }
    if (w > r) {
        rta.w = r;
        rta.h = (rta.w * h) / w;
    }
    return rta;
}

function snippet_scale_restrictH(w, h, r) {
    var rta = {
        w: w,
        h: h
    };
    if (h > r) {
        rta.h = r;
        rta.w = (rta.h * w) / h;
    }
    return rta;
}

function snippet_scale_restrictAuto(w, h, rw, rh) {
    var r = null;
    if (w >= h) {
        if (w > rw) {
            r = snippet_scale_restrictW(w, h, rw);
            var rta = snippet_scale_restrictH(r.w, r.h, rh);
            return rta;
        }
    } else {
        if (h > rh) {
            r = snippet_scale_restrictH(w, h, rh);
            var rta = snippet_scale_restrictW(r.w, r.h, rw);
            return rta;
        }
    }
}

function snippet_evt_on(elem, event, fn) {
    function listenHandler(e) {
        var ret = fn.apply(this, arguments);
        if (ret === false) {
            e.stopPropagation();
            e.preventDefault();
        }
        return (ret);
    }

    function attachHandler() {
        var ret = fn.call(elem, window.event);
        if (ret === false) {
            window.event.returnValue = false;
            window.event.cancelBubble = true;
        }
        return (ret);
    }
    if (elem.addEventListener) {
        elem.addEventListener(event, listenHandler, false);
        return listenHandler;
    } else {
        elem.attachEvent("on" + event, attachHandler);
        return attachHandler;
    }
}
function eventable(o){
    var events = {};
    o.$on   = function(n,fn){
        events[n] = events[n] || [];
        var id = new Date().getTime();
        events[n].push({id:id,fn:fn});
        return {name:n,id:id};
    };
    o.$length = function(n){
        events[n] = events[n] || [];
        return events[n].length;
    };
    o.$emit = function(n,p){
        events[n] = events[n] || [];
        for(var x in events[n]){
            console.info(n+'{id:'+events[n][x].id+'}');
            events[n][x].fn(p);
        }
        if(events[n].length === 0){
            console.info(n+'(zero bindings)');
            return false;
        }else{
            return true;
        }
    };
    o.$one = function(n,p){
        events[n] = events[n] || [];
        for(var x; x < events[n];x++){
            events[n][x].fn(p);
            delete events[n][x];
        }
    };
    o.$off = function(onrta){
        events[onrta.name] = events[onrta.name] || [];
        for(var x; x < events[onrta.name];x++){
            if(events[onrta.name][x].id==onrta.id){
                delete events[onrta.name][x];
            }
        }
    };
    o.$info = function(){
        for(var x; x < events;x++){
            var evt = events[x];
            console.info(x + ' ('+evt.length+')');
        }
    }
    return o;
}
eventable(top.window);
