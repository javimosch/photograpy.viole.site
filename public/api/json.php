<?php


Flight::route('POST /editor/save', function(){
  $data = json_decode(file_get_contents('php://input'), true);
  //backup
  $config = json_decode(file_get_contents(CONFIG),true);
  date_default_timezone_set('America/Argentina/Buenos_Aires');
  $backup_filename = 'backups/config.'.date('Y_m_d_H_i_s').'.json';
  file_force_contents($backup_filename, base64_encode(file_get_contents(CONFIG)));
  //	save
  file_put_contents($data['filename'], json_encode($data['json']));
  //
  echo json_encode(array(
  	"ok"=>true,
  	"$backup_filename"=>$backup_filename
  ));
  exit;
  //Flight::render('editor/save'.PREFIX.'.es', array('lang'=>'es'));
});

function file_force_contents($filename, $data, $flags = 0){	
    if(!is_dir(dirname($filename)))
        mkdir(dirname($filename).'/', 777, TRUE);
    return file_put_contents($filename, $data,$flags);
}




?>