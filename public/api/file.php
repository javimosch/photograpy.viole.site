<?php


Flight::route('POST /file/upload', function(){
    $data = json_decode(file_get_contents('php://input'), true);
    //


	//
    $r = array(
    	"ok"=>true,
    	"data"=> Flight::upload()
    	);
    echo json_encode($r);
    exit;
});

Flight::map('upload',function(){



	if (!empty($_FILES['fd-file']) and is_uploaded_file($_FILES['fd-file']['tmp_name'])) {
	  // Regular multipart/form-data upload.
	  $name = $_FILES['fd-file']['name'];
	  $data = file_get_contents($_FILES['fd-file']['tmp_name']);
	} else {
	  // Raw POST data.
	  $name = urldecode(@$_SERVER['HTTP_X_FILE_NAME']);
	  $data = file_get_contents("php://input");
	}
	$output = sprintf('%s; received %s bytes, CRC32 = %08X, MD5 = %s', $name,
	                  number_format(strlen($data)), crc32($data), strtoupper(md5($data)));
	$opt = &$_REQUEST['upload_option'];
	isset($opt) and $output .= "\nReceived upload_option with value $opt";
	// return $output;

return json_encode($data);

$target_dir = "uploads/";
$target_file = $target_dir . basename($name);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($data);
    if($check !== false) {
        //echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        return "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    return "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fd-file"]["size"] > 500000) {
    return "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    return "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    return "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fd-file"]["tmp_name"], $target_file)) {
        return "The file ". basename( $_FILES["fd-file"]["name"]). " has been uploaded.";
    } else {
        return "Sorry, there was an error uploading your file.";
    }
}

});

?>