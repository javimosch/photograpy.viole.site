<?php include 'partials/top.base.php';?>

<div class="photoContainer">
<div class="slide-boxes"><div class="left"></div><div class="right"></div></div>
<?php 
if($id!=-1){
	$data = file_get_contents (CONFIG);
	$json = json_decode($data, true);
	foreach ($json['grupos'] as $key => $o) {
		
		if(str_replace(" ","_",$o['label'])==$id){
		
			foreach ($o['photos'] as $key => $photo) {
				$src = $photo['src'];
				$active = ($key==0)?'':'';
				echo "<img class=' photo ".$active."' src='../".$src."' "
				." alt='".$photo['description']."'"
				."/>";
			}
			
		}
	}
}
?>
</div>

<?php include 'partials/bottom.base.php';?>
