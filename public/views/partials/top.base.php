<?php
	
	$data = file_get_contents (CONFIG);
	$json = json_decode($data, true);
	$js   = ""
	."window.config=".$data.";"
	."window.site={"
		."config:".$data.","
		."path:'".PATH."',"
		."active:'".$active."'"
	."};";
	//
	define('TITLE',$json['titlePrefix']);
?>

<!DOCTYPE html>
<html lang="en" class=" desktop landscape">
<head>
	<title>Inicio <?=TITLE?></title>
	<meta name="description" content="">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<meta name="format-detection" content="telephone=no">
	<link rel="shortcut icon" type="image/x-icon" href="<?=PATH?>img/favicon.ico">
	<link rel="icon" href="<?=PATH?>img/favicon.ico" type="image/x-icon">
	
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Roboto:400,100,300,500,700,900|Lato:400,100,300,700,900|Source+Sans+Pro:400,200,300,700,600,900|Poiret+One|Chivo:400,900' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?=PATH?>css/jsoneditor.min.css">
	<link rel="stylesheet" type="text/css" href="<?=PATH?>css/animate.css">
	<link rel="stylesheet" type="text/css" href="<?=PATH?>css/style.css">

	<script type="text/javascript"><?php echo $js?></script>
	 
</head>
<body>

	<?php include 'header.base.php';?>
